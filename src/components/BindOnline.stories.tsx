import React from 'react';
import QuestionOriginal from '../images/bind/question_original.png';
import QuestionMarginSm from '../images/bind/question_margin_sm.png';

export default {
  title: 'Layout/Bind Online',
};

export const Basic = () => (
  <div>
    <img src={QuestionOriginal} style={{ width: '100%', height: 'auto' }} />
  </div>
);
